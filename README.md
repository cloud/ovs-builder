# OVS Builder

Pipeline for building OpenVSwitch RPMs from git source tree. Builds both userspace binaries and kernel modules.

## Usage
The build pipeline is controlled with the following variables:

```bash
OVS_SOURCE=https://github.com/openvswitch/ovs.git
OVS_BRANCH=2.9
KVER=3.10.0-862.9.1.el7.x86_64
```

### Version override

Run pipeline with one of the following variable set `SET_BUILD_VERSION` or `SET_RELEASE_VERSION`.

For in order to achieve version `2.11.93-7` set the variables:

```bash
SET_BUILD_VERSION=7
SET_RELEASE_VERSION=2.11.93
```
Defaults:
```bash
SET_BUILD_VERSION=1
SET_RELEASE_VERSION=2.12.0 # or the curent version
```
